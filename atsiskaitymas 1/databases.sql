/*
Užduotis studentui mast3898:

1
Visų konkrečioje leidykloje išleistų knygų skaičius.
*/

SELECT COUNT(*) FROM stud.knyga WHERE leidykla = 'Baltoji';

/*
2
Konkretaus skaitytojo, nurodyto jo AK, bendra visų jo skaitomų egzempliorių vertė.
*/

SELECT verte
FROM stud.skaitytojas as s 
JOIN stud.egzempliorius egz ON (egz.skaitytojas = s.nr)
JOIN stud.knyga ON (knyga.isbn = egz.isbn)
WHERE ak = '38105301031'
ORDER BY verte;

/*
3
Kiekvienai autoriaus pavardei bendrapavardžių autorių skaičius ir bendras visų jų knygų puslapių skaičius.
*/

SELECT pavarde, COUNT(DISTINCT vardas) - 1 AS bendrapavardziu, sum(puslapiai) AS viso_puslapiu
FROM stud.autorius AS aut
JOIN stud.knyga knyga ON (knyga.isbn = aut.isbn)
GROUP BY pavarde
HAVING count(*) > 1;

/*
4
Pavadinimai knygų, kurių egzempliorių bibliotekoje yra mažiau už visų knygų egzempliorių skaičių vidurkį.
*/

WITH sk_tbl AS (
    SELECT COUNT(*) AS total 
    FROM stud.egzempliorius 
    GROUP BY isbn
),
    vidurkis_tbl AS (
    SELECT avg(total) AS vidurkis FROM sk_tbl
)
SELECT pavadinimas, knyga.isbn
FROM stud.knyga AS knyga
JOIN stud.egzempliorius egz ON (knyga.isbn = egz.isbn)
GROUP BY knyga.isbn
HAVING count(*) < (SELECT vidurkis FROM vidurkis_tbl);



/*
5
Visos lentelės, kuriose nėra nei vieno stulpelio, galinčio įgyti NULL reikšmę.
*/

SELECT DISTINCT table_name
FROM information_schema.columns
EXCEPT 
    SELECT DISTINCT table_name
    FROM information_schema.columns
    WHERE is_nullable = 'YES';


SELECT DISTINCT table_name
FROM information_schema.columns
WHERE table_name NOT IN (
    SELECT DISTINCT table_name
    FROM information_schema.columns
    WHERE is_nullable = 'YES'
)


/*

   STRUCTURE
test1.knyga
test2.knyga
stud.skaitytojas
stud.egzempliorius
stud.knyga
stud.autorius

*/